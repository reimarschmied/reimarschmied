import { YamaAppPage } from './app.po';

describe('yama-app App', () => {
  let page: YamaAppPage;

  beforeEach(() => {
    page = new YamaAppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
